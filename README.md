# Powiekszony nagłówek
paragraf 1

paragraf 2

paragraf 3

# przekreslenie pogrubienie kursywa
~~przekreślenie~~
**pogrubienie**
*kursywa*

# cytaty
cytat
>cytat 1
>>cytat 2
>>>cytat 3

# lista numeryczna
1. Jeden
2. Dwa
3. Trzy

# lista nienumeryczna
* jeden
+ dwa
- trzy

# blok pogramu

```py
list=[1,2,'a','b']
list.append[3]
print(list[4])
```



# kod programu w tekscie
Witaj Świecie to po angielsku `print("Hello World")`

# zdjecie
![kanye.png](kanye.png)






